package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.ICommandRepository;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command VERSION = new Command(
            TerminalConst.CMD_VERSION,
            ArgumentConst.CMD_VERSION,
            "Shows program version"
    );

    private final static Command INFO = new Command(
            TerminalConst.CMD_INFO,
            ArgumentConst.CMD_INFO,
            "Shows system info"
    );

    private final static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT,
            ArgumentConst.CMD_ABOUT,
            "Shows information about developer"

    );

    private final static Command HELP = new Command(
            TerminalConst.CMD_HELP,
            ArgumentConst.CMD_HELP,
            "Shows this message"
    );

    private final static Command EXIT = new Command(
            TerminalConst.CMD_EXIT,
            null,
            "Exit application"
    );

    private final static Command ARG = new Command(
            TerminalConst.CMD_ARG,
            ArgumentConst.CMD_ARG,
            "Shows list of arguments"
    );

    private final static Command CMD = new Command(
            TerminalConst.CMD_CMD,
            ArgumentConst.CMD_CMD,
            "Shows list of commands"
    );

    private final static Command PRJ_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR,
            null,
            "Clear all projects"
    );

    private final static Command PRJ_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE,
            null,
            "Create new project"
    );

    private final static Command PRJ_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST,
            null,
            "Show available projects"
    );

    private final static Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST,
            null,
            "Show available tasks"
    );

    private final static Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE,
            null,
            "Create new task"
    );

    private final static Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR,
            null,
            "Clear all tasks"
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, ARG, CMD, PRJ_CLEAR, PRJ_CREATE, PRJ_LIST, TASK_CLEAR, TASK_CREATE, TASK_LIST, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
