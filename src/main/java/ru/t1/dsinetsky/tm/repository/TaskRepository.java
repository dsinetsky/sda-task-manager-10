package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.ITaskRepository;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task createTask(Task task){
        tasks.add(task);
        return task;
    }

    @Override
    public void clearTasks(){
        tasks.clear();
    }

    @Override
    public List<Task> returnAll(){
        return tasks;
    }

}
