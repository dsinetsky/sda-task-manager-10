package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.IProjectRepository;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> returnAll(){
        return projects;
    }

    @Override
    public Project add(final Project project){
        projects.add(project);
        return project;
    }

    @Override
    public void clearAll(){
        projects.clear();
    }

}
