package ru.t1.dsinetsky.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_VERSION = "version";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_ARG = "arg";

    public static final String CMD_CMD = "cmd";

    public static final String CMD_PROJECT_CLEAR = "prj-clear";

    public static final String CMD_PROJECT_CREATE = "prj-create";

    public static final String CMD_PROJECT_LIST = "prj-show";

    public static final String CMD_TASK_CLEAR = "task-clear";

    public static final String CMD_TASK_CREATE = "task-create";

    public static final String CMD_TASK_LIST = "task-show";

}
