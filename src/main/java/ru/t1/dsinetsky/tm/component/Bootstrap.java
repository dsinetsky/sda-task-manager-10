package ru.t1.dsinetsky.tm.component;

import ru.t1.dsinetsky.tm.api.*;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.controller.CommandController;
import ru.t1.dsinetsky.tm.controller.ProjectController;
import ru.t1.dsinetsky.tm.controller.TaskController;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;
import ru.t1.dsinetsky.tm.repository.TaskRepository;
import ru.t1.dsinetsky.tm.service.CommandService;
import ru.t1.dsinetsky.tm.service.ProjectService;
import ru.t1.dsinetsky.tm.service.TaskService;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void terminalRun(final String command) {
        if (command == null) {
            commandController.displayError();
            return;
        }
        switch (command) {
            case (TerminalConst.CMD_HELP):
                commandController.displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                commandController.displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                commandController.displayAbout();
                break;
            case (TerminalConst.CMD_INFO):
                commandController.displayInfo();
                break;
            case (TerminalConst.CMD_ARG):
                commandController.displayArguments();
                break;
            case (TerminalConst.CMD_CMD):
                commandController.displayCommands();
                break;
            case (TerminalConst.CMD_PROJECT_CLEAR):
                projectController.clear();
                break;
            case (TerminalConst.CMD_PROJECT_CREATE):
                projectController.create();
                break;
            case (TerminalConst.CMD_PROJECT_LIST):
                projectController.show();
                break;
            case (TerminalConst.CMD_TASK_CLEAR):
                taskController.clearTasks();
                break;
            case (TerminalConst.CMD_TASK_CREATE):
                taskController.createTask();
                break;
            case (TerminalConst.CMD_TASK_LIST):
                taskController.showTasks();
                break;
            case (TerminalConst.CMD_EXIT):
                exitApp();
                break;
            default:
                commandController.displayError();
                break;
        }
    }

    private void argumentRun(final String arg) {
        switch (arg) {
            case (ArgumentConst.CMD_HELP):
                commandController.displayHelp();
                break;
            case (ArgumentConst.CMD_VERSION):
                commandController.displayVersion();
                break;
            case (ArgumentConst.CMD_ABOUT):
                commandController.displayAbout();
                break;
            case (ArgumentConst.CMD_INFO):
                commandController.displayInfo();
                break;
            case (ArgumentConst.CMD_CMD):
                commandController.displayCommands();
                break;
            case (ArgumentConst.CMD_ARG):
                commandController.displayArguments();
                break;
            default:
                commandController.displayError();
                break;
        }
    }

    private boolean argRun(final String[] args) {
        if (args.length < 1) {
            return false;
        }
        String param = args[0];
        argumentRun(param);
        return true;
    }


    private void exitApp() {
        System.exit(0);
    }

    public void run(final String[] args) {
        if (argRun(args)) {
            exitApp();
            return;
        }
        commandController.displayWelcome();
        while (true) {
            System.out.println("\nEnter command:");
            String command = TerminalUtil.nextLine();
            terminalRun(command);
        }
    }

}
