package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.IProjectController;
import ru.t1.dsinetsky.tm.api.IProjectService;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void create(){
        System.out.println("Enter name of new project:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String desc = TerminalUtil.nextLine();
        Project proj = projectService.create(name,desc);
        if (proj == null) System.out.println("Error! Project cant be created");
        else System.out.println("Project successfully created");
    }

    @Override
    public void show(){
        System.out.println("List of projects:");
        List<Project> listProjects = projectService.returnAll();
        int index = 1;
        for (Project proj : listProjects) {
            System.out.println(index + ". " + proj.toString());
            index++;
        }
    }

    @Override
    public void clear(){
        projectService.clearAll();
        System.out.println("All projects successfully cleared");
    }

}
