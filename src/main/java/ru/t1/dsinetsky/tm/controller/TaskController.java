package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.ITaskController;
import ru.t1.dsinetsky.tm.api.ITaskService;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks(){
        System.out.println("List of tasks:");
        List<Task> listTasks = taskService.returnAll();
        int i = 1;
        for (Task task : listTasks) {
            System.out.println(i + ". " + task.toString());
            i++;
        }
    }

    @Override
    public void createTask(){
        System.out.println("Enter name of new task:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String desc = TerminalUtil.nextLine();
        Task task = taskService.create(name,desc);
        if (task == null) System.out.println("Error! Cant create new task");
        else System.out.println("Task successfully created");
    }

    @Override
    public void clearTasks(){
        taskService.clearAll();
        System.out.println("Tasks successfully cleared");
    }

}
