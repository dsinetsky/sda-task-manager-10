package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.IProjectRepository;
import ru.t1.dsinetsky.tm.api.IProjectService;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> returnAll() {
        return projectRepository.returnAll();
    }

    @Override
    public Project create(final String name){
        if (name == null && name.isEmpty()) return null;
        return add(new Project(name));
    }

    @Override
    public Project create(final String name, final String desc){
        if (name == null || name.isEmpty()) return null;
        if (desc == null || desc.isEmpty()) return null;
        return add(new Project(name,desc));
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public void clearAll() {
        projectRepository.clearAll();
    }

}
