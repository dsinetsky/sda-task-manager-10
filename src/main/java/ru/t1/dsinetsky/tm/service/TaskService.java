package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.ITaskRepository;
import ru.t1.dsinetsky.tm.api.ITaskService;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void clearAll(){
        taskRepository.clearTasks();
    }

    @Override
    public List<Task> returnAll(){
        return taskRepository.returnAll();
    }

    @Override
    public Task create(String name){
        if(name == null || name.isEmpty()) return null;
        return taskRepository.createTask(new Task(name));
    }

    @Override
    public Task create(String name, String desc){
        if(name == null || name.isEmpty()) return null;
        if(desc == null || desc.isEmpty()) return null;
        return taskRepository.createTask(new Task(name,desc));
    }
}
