package ru.t1.dsinetsky.tm.api;

import ru.t1.dsinetsky.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> returnAll();

    Project create(String name);

    Project create(String name, String desc);

    Project add(Project project);

    void clearAll();

}
