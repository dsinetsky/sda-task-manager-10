package ru.t1.dsinetsky.tm.api;

public interface IProjectController {

    void create();

    void show();

    void clear();

}
