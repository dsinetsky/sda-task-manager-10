package ru.t1.dsinetsky.tm.api;

public interface ITaskController {
    void showTasks();

    void createTask();

    void clearTasks();
}
