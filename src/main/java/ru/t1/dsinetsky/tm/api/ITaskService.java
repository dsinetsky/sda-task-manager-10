package ru.t1.dsinetsky.tm.api;

import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskService {
    void clearAll();

    List<Task> returnAll();

    Task create(String name);

    Task create(String name, String desc);
}
