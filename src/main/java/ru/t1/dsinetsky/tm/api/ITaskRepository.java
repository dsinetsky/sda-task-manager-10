package ru.t1.dsinetsky.tm.api;

import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task createTask(Task task);

    void clearTasks();

    List<Task> returnAll();

}
