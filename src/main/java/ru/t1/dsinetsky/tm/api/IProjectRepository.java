package ru.t1.dsinetsky.tm.api;

import ru.t1.dsinetsky.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> returnAll();

    Project add(Project project);

    void clearAll();

}
